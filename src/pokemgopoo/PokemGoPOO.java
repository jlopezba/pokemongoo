package pokemgopoo;

import funcionalidad.Ataque;
import funcionalidad_tipos.BatallaAleatoria;

/**
 *
 * @author milton
 */
public abstract class PokemGoPOO {

    //Atributos en private
    private String nombre;
    private int vida;
    private int energia;
    public int dañoBase;
    public int defensa;
    public int precision;

    private Ataque[] ataques = new Ataque[4];

    public PokemGoPOO(int minimo, int maximo, String nombre, int vida) {
        super();
        setNombre(nombre);
        setVida(vida);
        setEnergia(100);
        generarAtaques(minimo, maximo);
    }

   
    public int getDañoBase() {
        return dañoBase;
    }

    public void setDañoBase(int dañoBase) {
        this.dañoBase = dañoBase;
    }

    public int getDefensa() {
        return defensa;
    }

    public void setDefensa(int defensa) {
        this.defensa = defensa;
    }

    public int getPrecision() {
        return precision;
    }

    public void setPrecision(int precision) {
        this.precision = precision;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        
        this.nombre = nombre;
    }

    public int getVida() {
        return vida;
    }

    public void setVida(int vida) {
        this.vida = vida;
    }

    public int getEnergia() {
        return energia;
    }

    public void setEnergia(int energia) {
        if (energia < 0) {
            System.out.println(" PERDIO EL DUELO POKEMON ");
        }

        this.energia = energia;

    }

    protected void generarAtaques(int minimo, int maximo) {
        int i = 0;

        ataques[i] = Ataque.values()[BatallaAleatoria.generarAleatorio(minimo, maximo)];
        for (i = 1; i < ataques.length; i++) {
            ataques[i] = Ataque.values()[BatallaAleatoria.generarAleatorio(minimo, maximo)];
            for (int j = 0; j < i; j++) {
                if (ataques[i] == ataques[j]) {
                    i--;
                }
            }
        }
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        PokemGoPOO other = (PokemGoPOO) obj;
        if (nombre == null) {
            if (other.nombre != null) {
                return false;
            }
        } else if (!nombre.equalsIgnoreCase(other.nombre)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return nombre;
    }

    public int compareTo(PokemGoPOO o) {
        return getNombre().compareTo(o.getNombre());
    }

    public Ataque getAtaques(int indice) {
        if (indice >= 0 && indice <= 4) {
            return ataques[indice];
        }
        return null;
    }

    public abstract int getAtaque(Ataque ataque);

}
